# UserScripts

Collection d'userscripts que j'ai crée ou forké pour répondre à des besoins personnels à un instant donné.

**Ils ne sont pas forcément achevés, ou maintenu à jour.**

## Liste des scripts

### Console test
Script sans grande utilité, servant juste à vérifier que les textes envoyés vers la console s'affichent correctement.

### Doc ubuntu-fr: Délai depuis maj en haut
Sur le wiki d'ubuntu-fr.org, affiche en haut des pages le délai écoulé depuis la dernière modification de la page. 
Permet ainsi de voir rapidement les pages qui ne sont plus très fraiches (scroller toute la page, c'est chiant).

### USO: Add favorite button on script pages
Sur userscript.org. Déplace ou ajoute le bouton "Favorite this script" près du bouton "Install" sur les différentes pages d'un script (About, Source code, Reviews, Discussions, etc...)